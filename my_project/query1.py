import sqlite3
from generateSample import *
def userSelect(social, interests, habits, misc, Net_ID):
	total = social + interests + habits + misc
	conn = sqlite3.connect("roommates.db")
	cur = conn.cursor()
	params = []

	for weight in getWeights(total):
		params.append(weight)
		params.append(Net_ID)
	params.append(Net_ID)
	params = tuple(params)
	
	view_string = 'CREATE TEMP VIEW Joined_Attributes AS\n'
	view_string = view_string + 'SELECT '
	first_cat = 1
	paramlist = [social,interests,habits,misc]
	if (social == 1):
		view_string = view_string + 'SOCIAL.*'
		first_cat = 0
	if interests == 1:
		if first_cat == 1:
			view_string = view_string + 'Interests.*'
			first_cat = 0
		else:
			view_string = view_string + ', Interests.*'
	if habits == 1:
                if first_cat == 1:
                        view_string = view_string + 'Habits.*'
                        first_cat = 0
                else:
                        view_string = view_string + ', Habits.*'
	if misc == 1:
                if first_cat == 1:
                        view_string = view_string + 'Misc.*'
                        first_cat = 0
                else:
                        view_string = view_string + ', Misc.*'
	view_string+= '\nFROM '
	first_cat = 1
	if (social == 1):
                view_string = view_string + 'Social\n'
                first_cat = 0
        if interests == 1:
                if first_cat == 1:
                        view_string = view_string + 'Interests'
                        first_cat = 0
                else:
                        view_string = view_string + '\tINNER JOIN Interests\n\t ON Interests.Net_ID = Social.Net_ID\n'
        if habits == 1:
                if first_cat == 1:
                        view_string = view_string + 'Habits\n'
                        first_cat = 0
                elif (first_cat == 0 and paramlist[1] == 1): 
                        view_string = view_string + '\tINNER JOIN Habits\n\t ON Habits.Net_ID = Interests.Net_ID\n'
		else:
			view_string = view_string + '\tINNER JOIN Habits\n\t ON Habits.Net_ID = Social.Net_ID\n'
        if misc == 1:
                if first_cat == 1:
                        view_string = view_string + 'Misc\n'
                        first_cat = 0
                elif first_cat == 0 and paramlist[2] == 1:
                        view_string = view_string + '\tINNER JOIN Misc\n\t ON Misc.Net_ID = Habits.Net_ID\n'
                elif first_cat == 0 and paramlist[1] == 1:
                        view_string = view_string + '\tINNER JOIN Misc\n\t ON Misc.Net_ID = Interests.Net_ID\n'
		else:
			view_string = view_string + '\tINNER JOIN Misc\n\t ON Misc.Net_ID = Social.Net_ID\n'
	view_string = view_string[:-1]
	sel_string = 'SELECT Net_ID, '
	if (social == 1):
		sel_string = sel_string + '(? * ABS(outgoing - (SELECT outgoing FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string+= '(? * ABS(party - (SELECT party FROM Joined_Attributes WHERE Net_ID = ?))) + \n'
		sel_string+= '(? * ABS(guestOver - (SELECT guestOver FROM Joined_Attributes WHERE Net_ID = ?))) + \n'
		sel_string+=    '(? * ABS(roomtime - (SELECT roomtime FROM Joined_Attributes WHERE Net_ID = ?)))\n'
		first_cat = 0
	if misc == 1:
		if first_cat ==0:
			sel_string+= '+ '
		sel_string+= '(? * ABS(cleanliness - (SELECT cleanliness FROM Joined_Attributes WHERE Net_ID = ?))) +\n'
		sel_string+= '(? * ABS(roomTemp - (SELECT roomTemp FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string+= '(? * ABS(pets - (SELECT pets FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string+= '(? * ABS(sharing - (SELECT sharing FROM Joined_Attributes WHERE Net_ID = ?)))\n' 
		first_cat = 0
	if interests == 1:
		if first_cat == 0:
			sel_string += '+ '
		sel_string+= '(? * ABS(sports - (SELECT sports FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string+=    '(? * ABS(games - (SELECT games FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string+=    '(? * ABS(tv - (SELECT tv FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string+=    '(? * ABS(cooking - (SELECT cooking FROM Joined_Attributes WHERE Net_ID = ?)))\n'
		first_cat = 0 
	if habits == 1:
		if first_cat == 0:
			sel_string+= '+ '
		sel_string += '(? * ABS(drinks - (SELECT drinks FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string += ' (? * ABS(smokes - (SELECT smokes FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string += '(? * ABS(studyAmount - (SELECT studyAmount FROM Joined_Attributes WHERE Net_ID = ?))) +\n' 
		sel_string += '(? * ABS(timeBed - (SELECT timeBed FROM Joined_Attributes WHERE Net_ID = ?)))'
	sel_string+= ' AS total FROM Joined_Attributes WHERE Net_ID != ? ORDER BY total DESC LIMIT 5'	
	print(view_string)
	cur.execute(view_string)
	print sel_string
	cur.execute(sel_string, params)
	results = cur.fetchall()
	print results
	# return results

userSelect(1, 1, 1, 1, "miller13")
